	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="../${pageContext.request.contextPath}/css/styleindex.css" />

    <title>Index</title>
  </head>

  <body>
    <!-- ***********************  HEADER / NAVIGATION *********************** -->
    <header class="header">
      <div class="container logo-nav-container">
        <a href="#" class="logo">LOGO</a>
        <!-- This span is in order to deploy the menu nav bar when the screen is small -->
        <span class="menu-icon">Menu</span>
        <nav class="">
          <ul class="navigation">
            <li style="color: blue;"><a href="Home.html" data-section="Home">Home</a></li>
            <li><a href="#">Login</a>
              <ul class="sub-nav"> 
                <li><a href="loginadmin.html" data-section="Form">Admin</a></li>
                <li><a href="loginuser.html">User</a></li>
              </ul>  
            <li><a href="#">Signup</a>
            <li><a href="#">Contact</a></li>
           
          </ul>
        </nav>
      </div>
    </header>
    <!-- ***********************  BODY *********************** -->

    <main class="main">
      <section class="Login_banner"><h1>User Login</h1></section>    
            
  <section class="admin-form">
    <div class="Login">
      <h1>Login</h1>
    </div>
    <form
    class="main-form"
    action="student.html"
    method="post"
    autocomplete="on"
  >
  <label for="user"> User </label>
  <input
    class="form-item"
    id="user"
    type="text"
    name="User"
    placeholder="User"
    required
  />
         

  <label for="password">Password </label>
  <input
    class="form-item"
    id="password"
    type="password"
    name="password"
    placeholder="Password"
  />
  

  <!-- intentar cambiar esto por un button que seria lo correcto -->
  <input
    class="button"
    type="submit"
    name="register"
    placeholder="SignIn"
    id="submit-button"
  />
</form>
 

  </section>
      
    </main>
    <!-- ***********************  FOOTER *********************** -->
    <footer class="footer">
      <div class="contact-info">
        <p>
          <strong>Contact info</strong>
          <br />
          Place
          <br />
          Queretaro
          <br />
          Other places
          <br />
          || CDMX || Guadalajara
          <br />
          Phone
          <br />
          2461103631
        </p>
      </div>
      <div class="copyright-container">
        <ul class="copyright-list">
          <li>© 2020 Test page for learning</li>
          <li><a href="Home.html" data-section="Home">|| Index</a></li>
          <li>
            <a href="form.html" data-section="Form">|| Form</a>
          </li>
        </ul>
      </div>
    </footer>
  </body>
</html>