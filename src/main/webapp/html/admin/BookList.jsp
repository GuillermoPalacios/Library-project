	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--  -->
 <link rel="stylesheet"  type="text/css" href="..${pageContext.request.contextPath}/css/style.css" />
<title>Home</title>
</head>
<body>
  <!-- ***********************  HEADER / NAVIGATION *********************** -->
 <header class="header">
      <div class="container logo-nav-container">
        <a href="#" class="logo">LOGO</a>
        <!-- This span is in order to deploy the menu nav bar when the screen is small -->
        <span class="menu-icon">Menu</span>
        <nav class="">
          <ul class="navigation">
            <li><a href="index.html" data-section="Home">Index</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Services</a>
            <ul class="sub-nav"> 
              <li><a href="RegisterForm.html">Add Book</a></li>
              <li><a href="SearchBook.html">Search a Book</a></li>
              <li><a href="BookList.html">Display Books</a></li>
              <li><a href="DeleteBook.html">Delete Book</a></li>
              <li><a href="UpdateBook.html">Update Book</a></li>
            </ul>
          </li>
            <li><a href="#">Contact</a></li>
           
          </ul>
        </nav>
      </div>
    </header>
        <!-- ***********************  BODY *********************** -->
 <section class="register-form">
   <center>
		<h1>BookManagment</h1>
        <h2>
        	<a href="RegisterForm.html">Add New Book</a>
        	&nbsp;&nbsp;&nbsp;
        	<a href="list">List All Books</a>
        	
        </h2>
	</center>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of Books</h2></caption>
            <tr>
                <th>LibraryCode</th>
                <th>Name</th>
                <th>Author</th>
                <th>ISBN</th>
                <th>Category</th>
                <th>Quantity</th>
            </tr>
            <c:forEach var="book" items="${listBooks}">
                <tr>
                    <td><c:out value="${book.library_code}" /></td>
                    <td><c:out value="${book.book_name}" /></td>
                    <td><c:out value="${book.author}" /></td>
                    <td><c:out value="${book.ISBN}" /></td>
                    <td><c:out value="${book.category}" /></td>
					<td><c:out value="${book.quantity}" /></td>
                    <td>
                    	<a href="edit?id=<c:out value='${book.library_code}' />">Edit</a>
                    	&nbsp;&nbsp;&nbsp;&nbsp;
                    	<a href="delete?id=<c:out value='${book.library_code}' />">Delete</a>                    	
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>	

 </section>

            <!-- ***********************  FOOTER *********************** -->
    <footer class="footer">
      <div class="contact-info">
        <p>
          <strong>Contact info</strong>
          <br />
          Place
          <br />
          Queretaro
          <br />
          Other places
          <br />
          ||CDMX || Guadalajara
          <br />
          Phone
          <br />
          2461103631
        </p>
      </div>
      <div class="copyright-container">
        <ul class="copyright-list">
          <li>© 2020 Test page for learning</li>
          <li><a href="Index.html" data-section="Home">|| Index</a></li>
          <li>
            <a href="form.html" data-section="Form">|| Form</a>
          </li>
        </ul>
      </div>
    </footer>
</body>