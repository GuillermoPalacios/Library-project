package edu.tcs.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.tcs.model.Admin;
import edu.tcs.model.Book;
import edu.tcs.repository.AdminRepository;
import edu.tcs.repository.BookRepository;

@WebServlet("/")
public class ListenerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private BookRepository bookRepository;
	private AdminRepository adminRepository;

	public void init() {
		bookRepository = new BookRepository();
		adminRepository = new AdminRepository();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());

		String action = request.getServletPath();
		try {
			switch (action) {
			case "/new":
				showNewForm(request, response);
				break;
			case "/register":
				insertBook(request, response);
				break;
			case "/delete":
				deleteBook(request, response);
				break;
			case "/edit":
				showEditForm(request, response);
				break;
			case "/update":
				updateBook(request, response);
				break;
			case "/list":
				listBook(request, response);
				break;

			case "/passwordSendEmail":
				sendEmail(request, response);
				break;
			case "/passwordReset":
				reviewCode(request, response);
				break;
			case "/newPassword":
				resetPassword(request, response);
				break;
			default:
				response.sendRedirect("index.html");
				break;
			}
		} catch (SQLException ex) {
			throw new ServletException(ex);
		}

	}

	private void resetPassword(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/Success.html");
		dispatcher.forward(request, response);
	}

	private void reviewCode(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {

		HttpSession session = request.getSession();
		Admin admin = (Admin) session.getAttribute("authcode");

		String code = request.getParameter("authcode");

		if (code.equals(admin.getCode())) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/passwordreset.html");
			dispatcher.forward(request, response);
		} else {
			System.out.println("Error");
		}
	}

	private void sendEmail(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		response.setContentType("text/html;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			// fetch form value
			Long id = Long.parseLong(request.getParameter("user_id"));

			String code = adminRepository.getRandom();
			// Create new admin using the id
			Admin admin = adminRepository.getAdmin(id);
			// I am setting the code that is going to be send
			adminRepository.updateCode(admin.getAdmin_id(), code);

			// call the send email method
			boolean test = adminRepository.sendEmail(admin);

			// check if the email send successfully
			if (test) {
				HttpSession session = request.getSession();
				session.setAttribute("authcode", admin);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/email.html");
				dispatcher.forward(request, response);
			} else {
				out.println("Failed to send verification email");
			}

		}

	}

	private void listBook(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		List<Book> listBooks = bookRepository.getAllBooks();
		request.setAttribute("listBooks", listBooks);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/BookList.html");
		dispatcher.forward(request, response);
	}

	private void showNewForm(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/RegisterForm.html");
		dispatcher.forward(request, response);
	}

	private void showEditForm(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		Book existingBook = bookRepository.getBook(id);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/RegisterForm.html");
		request.setAttribute("book", existingBook);
		dispatcher.forward(request, response);

	}

	private void insertBook(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// Adding a new book
		// Getting parameters from the jsp form
		String book_name = request.getParameter("bookName");
		String author = request.getParameter("author");
		String category = request.getParameter("category");
		int iSBN = Integer.parseInt(request.getParameter("ISBN"));
		int quantity = Integer.parseInt(request.getParameter("quantity"));
//		//Setting parameters in order to make some actions
		Book book = new Book();
		book.setBook_name(book_name);
		book.setAuthor(author);
		book.setCategory(category);
		book.setISBN(iSBN);
		book.setQuantity(quantity);

		bookRepository.addBook(book);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/Success.html");
		dispatcher.forward(request, response);

	}

	private void updateBook(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		String book_name = request.getParameter("bookName");
		String author = request.getParameter("author");
		String category = request.getParameter("category");
		int iSBN = Integer.parseInt(request.getParameter("ISBN"));
		int quantity = Integer.parseInt(request.getParameter("quantity"));

		Book book = new Book(book_name, author, iSBN, category, quantity);
		bookRepository.updateBook(book);
		response.sendRedirect("list");
	}

	private void deleteBook(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		Long id = Long.parseLong(request.getParameter("id"));
		bookRepository.deleteBook(id);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Success.html");
		dispatcher.forward(request, response);
	}

}