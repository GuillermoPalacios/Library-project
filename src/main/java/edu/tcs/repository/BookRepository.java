package edu.tcs.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;


import edu.tcs.model.Book;

import edu.tcs.utilities.JPA_Util;

public class BookRepository {
	
	EntityManager entity = JPA_Util.getEntityManagerFactory().createEntityManager();
	
	
	//Working properly
	public void addBook(Book book) {
		
        EntityTransaction transaction = entity.getTransaction();

        transaction.begin();
        //Here the date is in the format that I want to use
          
        entity.persist(book);
 
        //For createNativeQuery i have to used the name of my table
        transaction.commit();
          entity.close();
          
	}
	
	public Book getBook(Long id) {
		Book book = new Book();
		book = entity.find(Book.class, id);
		return book;
	}
	
	public void updateBook(Book book) {

		entity.getTransaction().begin();
		entity.merge(book);
		entity.getTransaction().commit();
		entity.close();
	}
	//Working properly
	public void deleteBook(Long id) {
		Book book = new Book();
		book = entity.find(Book.class, id);
		entity.getTransaction().begin();
		entity.remove(book);
		entity.getTransaction().commit();
		
	}
	
	
	public List<Book> getAllBooks() {

		List<Book> book_list = new ArrayList<Book>();
		
		Query query = entity.createQuery("FROM book_info");
		book_list=query.getResultList();
		
		return book_list;
		

	}
	
}
