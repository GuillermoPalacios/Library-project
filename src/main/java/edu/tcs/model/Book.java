package edu.tcs.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity(name="book_info")
@Table(name="book_info")
public class Book implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "library_code")
	private Long   library_code;
	@NotBlank
	@Column(name = "book_name", length = 50)
	private String book_name;
	@Column(name = "book_author", length = 50)
	private String author;
	@Column(unique = true, length = 13)
	private int ISBN;
	@Column(name = "book_category", length = 50)
	private String category;
	@Column(name="Quantity")
	private int quantity;
	
	
	
	public Book() {

	}
	public Book(String book_name, String author, int iSBN, String category, int quantity) {
	
		this.book_name = book_name;
		this.author = author;
		this.ISBN = iSBN;
		this.category = category;
		this.quantity = quantity;
	}
	public Long getLibrary_code() {
		return library_code;
	}
	public void setLibrary_code(Long library_code) {
		this.library_code = library_code;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getISBN() {
		return ISBN;
	}
	public void setISBN(int iSBN) {
		ISBN = iSBN;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	



}
