package edu.tcs.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.Email;

@Entity(name="Admin")
@Table(name="Admin")
public class Admin implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "admin_id")
	private Long   admin_id;
	
	@Column(name = "admin_name", length = 50)
	private String admin_name;
	@Column(name = "admin_password", length = 50)
	private String admin_password;
	@Email(message = "Email should be valid")
	@Column(name = "email", length = 50)
	private String email;
	@Column(name="code")
	private String code;
		
	public Admin() {
		super();
	}
	
	public Admin(String admin_name, String admin_password, String email) {
		super();
		this.admin_name = admin_name;
		this.admin_password = admin_password;
		this.email=email;
	}
	
	public Admin(String admin_name, String admin_password, String email,String code) {
		super();
		this.admin_name = admin_name;
		this.admin_password = admin_password;
		this.email=email;
		this.code=code;
	}
	public Long getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(Long admin_id) {
		this.admin_id = admin_id;
	}
	public String getAdmin_name() {
		return admin_name;
	}
	public void setAdmin_name(String admin_name) {
		this.admin_name = admin_name;
	}
	public String getAdmin_password() {
		return admin_password;
	}
	public void setAdmin_password(String admin_password) {
		this.admin_password = admin_password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	
	
	
	
	



}
